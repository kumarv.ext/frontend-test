import { Component, OnInit } from '@angular/core';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  detail:any
  constructor(private ss: ServicesService) { }

  ngOnInit(): void {
    this.detail=this.ss.detail
    console.log(this.detail)
  }

  copy(data:any){
    console.log("click",data.id)
    navigator.clipboard.writeText(data);
    // alert("Copied the text: " + data);
  }
}
