import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private ss: ServicesService, private router: Router) { }
  menu: any
  ngOnInit(): void {
  }

  tiredMenu(data:any) {
    let url = `https://od.pxsweb.com/api/v1/restaurants/${data}/menu/tier?key=87056311b113a42b5d2d5eae4e21dba000be03b9`
    this.ss.get(url).subscribe(data => {
      this.menu = data
      console.log(data)
    })
  }

  post() {
    let newData = {
      "EMAIL_ADDRESS": "example@gmail.com",
      "DEVICE_MAC_ADDRESS": "10-6F-D9-A6-AF-36",
      "IS_VERIFIED_EMAIL": true,
      "FIRST_NAME": "Test",
      "LAST_NAME": "User",
      "MOBILE_NUMBER": "+917206123456",
      "DATE_OF_BIRTH": "2022-09-22",
      "LANGUAGE": "English",
      "SIGNED_UP_TIMESTAMP": "2022-10-05T11:07:21.257Z",
      "FIRST_LOGGED_IN_TIMESTAMP": "2022-10-05T11:07:21.257Z",
      "LAST_LOGGED_IN_TIMESTAMP": "2022-10-05T11:07:21.257Z",
      "IS_OPTED_IN_FOR_SHARE_APP_ACTIVITY": true,
      "IS_OPTED_IN_FOR_PUSH_NOTIFICATIONS": true,
      "IS_OPTED_IN_FOR_EMAIL_NOTIFICATIONS": true,
      "IS_OPTED_IN_FOR_SMS_NOTIFICATIONS": true,
      "IS_LEGACY_USER": true,
      "IS_LEGACY_USER_FIRST_LOGIN": true,
      "CURRENT_DEVICE_PLATFORM": "Android",
      "CURRENT_APP_VERSION": "6.0.1",
      "MISCELLANEOUS": "{}",
      "CITY_NAME": "Toronto",
      "PROVINCE_NAME": "Canada",
      "CREATED_BY": "Saurabh",
      "UPDATED_BY": "Saurabh"
    }
    this.ss.post(newData).subscribe(data => {
      console.log(data)
    })
  }

  send(data: any) {
    this.ss.items=data.items
    this.router.navigate(['/items'])
  }

}
