import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesService } from 'src/app/services/services.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  constructor(private ss: ServicesService,private router: Router) { }
  allItem:any
  ngOnInit(): void {
    this.allItem=this.ss.items
    console.log(this.ss.items)
  }

  copy(data:any){
    console.log("click",data.id)
    navigator.clipboard.writeText(data.id);
    // alert("Copied the text: " + data);
  }
  showDetails(data:any){
    console.log(data)
    this.ss.detail=data
    this.router.navigate(['/item'])
  }

}
