import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ServicesService {
  items:any
  detail:any
  constructor(private http:HttpClient) { }

  post(data:any){
    return this.http.post('http://localhost:8000/user',data)
  }

  get(url:any){
    return this.http.get(url)
  }
}
